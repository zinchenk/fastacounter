#!/usr/bin/env bash

# pass in a list of FASTA file names

for filename in $@
do
      echo $filename: $(egrep -c "^>" $filename)  
done
